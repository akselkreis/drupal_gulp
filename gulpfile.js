'use strict';

var gulp = require('gulp'),
  notify = require('gulp-notify'),
  autoprefixer = require('gulp-autoprefixer'),
  concat = require('gulp-concat'),
  combinemq = require('gulp-group-css-media-queries'),
  imagemin = require('gulp-imagemin'),
  jshint = require('gulp-jshint'),
  stylish = require('jshint-stylish'),
  minifycss = require('gulp-minify-css'),
  rename = require('gulp-rename'),
  sass = require('gulp-ruby-sass'),
  shell = require('gulp-shell'),
  size = require('gulp-size'),
  uglify = require('gulp-uglify'),
  pngcrush = require('imagemin-pngcrush'),
  maps = require('gulp-sourcemaps'),
  del = require('del'),
  stripmq = require('gulp-stripmq'),
  inject = require('gulp-inject');

var config = {
  sass: './scss/**/*.scss',
  css: './css',
  css_nomq: './css/nomq',
  js: './js',
  images: './images/*.{png,gif,jpeg,jpg,svg}',
  imagesmin: './images/minified',
  templates: './templates',
};

var autoprefixer_browsers = [
  '> 3%'
];

gulp.task('styles-nomq', function() {
  return sass(config.sass, {
    style: 'expanded',
    precision: 6
  })
    .pipe(stripmq({
      options: {width: 1024},
      type: 'screen'
    }))
    .pipe(autoprefixer({
      browsers: autoprefixer_browsers,
      cascade: false
    }))
    .pipe(gulp.dest(config.css_nomq))
    .pipe(minifycss())
    .pipe(rename({suffix: '.min'}))
    .pipe(gulp.dest(config.css_nomq + '/minified'))
    .pipe(size({title: 'css-nomq'}));
});

gulp.task('images', function() {
  return gulp.src(config.images)
    .pipe(imagemin({
      optimizationLevel: 7,
      progressive: true,
      interlaced: true
    }))
    .pipe(gulp.dest(config.imagesmin))
    .pipe(size({title: 'images'}));
});

// gulp.task('drush', shell.task([
//   'drush cc theme-registry'
// ]));

// gulp.task('default', ['styles-nomq', 'styles', 'productionjs', 'images']);

gulp.task('watch', function() {
//  gulp.watch(config.sass, ['styles-nomq']);
  gulp.watch(config.sass, ['compileSass']);
  gulp.watch(config.js + '/*.js', ['concatScripts']);
//  gulp.watch(config.images, ['images']);
});

// gulp.task('watch-drush', ['watch'], function() {
//   gulp.watch('**/*.{php,inc,info}', ['drush']);
// });

// Tyler's contributions:

// Notes:
// gulp build - builds minified production files in a production directory
// gulp dev – concats javascript files and compiles sass
// gulp watch - watches scss and js for changes
// gulp clean – removes generated files
// gulp (default task) – runs gulp clean, then dev, build, and watch
gulp.task('compileSass', function(){
  return sass(config.sass, {
    style: 'expanded',
    precision: 6,
    sourcemap: true
  })
  .pipe(maps.init())
  .pipe(rename('style.css'))
  .pipe(maps.write('./'))
  .pipe(gulp.dest('css'))
});

gulp.task('minifySass', function(){
  gulp.src('css/style.css')
  .pipe(maps.init())
  .pipe(combinemq())
  .pipe(autoprefixer({
    browsers: autoprefixer_browsers,
    cascade: false
  }))
  .pipe(minifycss())
  .pipe(gulp.dest('css'))
});

// Matt S - Need to update the path for Template file to match the new drupal theme
gulp.task('criticalCSS', function() {
  gulp.src([config.templates + 'index.src.html'])

    .pipe(inject(gulp.src(['./css/minified/critical.min.css']), {
      removeTags: 'true',
      starttag: '<!-- inject:criticalCSS:{{ext}} -->',
      transform: function (filePath, file) {
        // return file contents as string
        return file.contents.toString('utf8')
      }
    }))
    .pipe(rename('index.html'))
    .pipe(gulp.dest(''));
});

gulp.task('concatScripts', function(){
  return gulp.src([
    config.js + '/libs/fastclick.js',
    config.js + '/libs/owl.carousel.min.js',
    config.js + '/libs/tableit.js',
    config.js + '/libs/jquery.mediaWrapper.js',
    config.js + '/*.js',
  ])
  .pipe(maps.init())
    .pipe(concat('production.js'))
    .pipe(maps.write('./'))
    .pipe(gulp.dest('js/build'))
});

gulp.task('minifyScripts', ['concatScripts'], function(){
  return gulp.src('js/build/production.js')
  .pipe(uglify())
  .pipe(gulp.dest(config.js + '/build'))
  .pipe(size({title: 'production js'}));
});

gulp.task('clean', function(){
  del(['css', 'js/build', 'production']);
});

gulp.task('dev', ['concatScripts','compileSass']);

gulp.task('build',['styles-nomq', 'minifyScripts', 'minifySass', 'images'], function(){
  return gulp.src(['css/style.css', 'js/build/production.min.js'], {base: './'})
  .pipe(gulp.dest('production'));
});
// ----------------------

// This could probably be cleaned up, and it would definitely be faster to run 'dev', 'watch', and 'build' when necessary
// I'd like to have the default gulp task just run 'clean', then 'build,' but this is closer to our grunt implementation
gulp.task('default', ['clean'], function(){
  gulp.start(['build','dev','watch']);
});
